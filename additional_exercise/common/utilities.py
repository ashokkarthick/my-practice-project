from common import database as db

'''
# @python program
# @Name: utilities.py                                                                                  
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program that has utility methods for the given exercises.
'''

def create_table(create_table_query, connection):
    '''
    Title: to create table
    args: create table query and db connection
    return: None
    '''
    try:
        #creating a cursor object
        cursor = connection.cursor()
        cursor.execute(create_table_query)
        print("table created successfully")

    except Exception as ex:
        print("The error is: ", ex)
    
    finally:
        db.close_cursor(cursor)

def select_data(select_query, connection):
    '''
    Title: to get data
    args: select query and db connection
    return: fetched data
    '''
    fetched_data = []
    try:
        #creating a cursor object
        cursor = connection.cursor()
        cursor.execute(select_query)
        fetched_data = cursor.fetchall()
        print("data fetched successfully")
    except Exception as ex:
        print("The error is: ", ex)
    finally:
        db.close_cursor(cursor)
    return fetched_data
        
def check_null(data):
    """
    Checks if obtained data is non or null.
    args: check_data:
    return: Boolean
    """
    check_status = False
    if data is not None and data != "":
        check_status = True
    return check_status

def check_digit(value):
    '''
    Title:function to check the intitution code is integer or not
    args: str_data
    return: boolean status
    '''
    status = False
    if check_null(value) == True:
        value = value.strip()
        if value.isdigit():
            status = True
    return status

def validate_phone_num(phone_num):
    '''
    Title: function to validate ph_num
    args: str_data of ph_num
    return: boolean
    '''
    status = False
    if check_null(phone_num) == True and len(phone_num) >= 10:
        phone_num = phone_num.replace(" ", "")
        phone_num  = phone_num.replace(",", "")
        if len(phone_num) % 10 == 0 and phone_num.isdigit():
            status = True
    return status

def validate_email(email):
    '''
    Title: function to validate the email
    args: email string data
    return: boolean status
    '''
    status = False
    if check_null(email) == True:
        email = email.strip()
        if " " not in email and email.count("@") == 1 and 3 > email[email.index("@"):].count(".") > 0 and email[-1] != "." and email[0] != ".":
            status = True
    return status

def validate_date(date):
    '''
    Title: function to validate the date
    args: str_data of date
    return: boolean
    '''
    status = False
    if check_null(date) == True:
        date = date.strip()
        if date.count("/") == 2:
            split_list = date.split("/")
            if len(split_list) == 3:
                if all(values.isdigit() for values in split_list):
                    if  0 < int(split_list[0]) <= 31 and 0 < int(split_list[1]) <= 12 and 1900 < int(split_list[2]):
                        status = True
    return status

def check_gender(gender):
    '''
    Title: check the gender 
    args: str_data
    return: boolean status
    '''
    status = False
    if gender == "M" or gender == "F":
        status = True
    return status

def check_decimal_value(value,digits_before_point):
    '''
    Title: get the decimal data
    args: str_data, decimals before point
    return: boolean
    '''
    status = False
    try:
        if check_null(value) == True:
            float_data = float(value)
            split_list = str(float_data).split(".")
            if len(split_list[0]) <= digits_before_point:
                status = True
    except Exception as ex:
        print(ex)
    return status

def get_sales_quantity(prod_id, sales_data):
    '''
    Title: function to get sum of sales quantity for given product id
    args: product id and sales data
    return: sales quantity
    '''
    sales_quantity = 0
    for lp in sales_data:
        if prod_id == lp[0]:
            sales_quantity += lp[1]
    return sales_quantity

def get_date(date):
    '''
    Title: function to get data in dd/mm/yyyy format
    args: date
    return: boolean or date in dd/mm/yyyy format
    '''
    status = False
    if validate_date(date) == True:
        date = date.strip()
        split_list = date.split("/")
        date = split_list[0] 
        month = split_list[1]
        year = split_list[2]
        if len(split_list[0]) == 1:
            date = "0" + split_list[0]
        if len(split_list[1]) == 1:
            month = "0" + split_list[1]
        status = (date) + "/" + (month) + "/" + year
        
    return status

