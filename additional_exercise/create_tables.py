from common import constants as cons
from common import utilities as util
from common import database as db

'''
# @python program
# @Name: create_tables.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program to create the tables necessary.
'''
connection = db.get_connection()

try:
    util.create_table(cons.CREATE_SUPPLIER_QUERY, connection)
    util.create_table(cons.CREATE_PRODUCT_QUERY,connection)
    util.create_table(cons.CREATE_CUSTOMER_QUERY, connection)
    util.create_table(cons.CREATE_PURCHASE_QUERY, connection)
    util.create_table(cons.CREATE_SALES_QUERY,connection)
except Exception as ex:
    print(ex)
finally:
    db.close_connection(connection)
    print("database closed")