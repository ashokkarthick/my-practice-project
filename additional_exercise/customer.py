from common import constants as cons
from common import utilities as util
from common import database as db

'''
# @python program
# @Name: customer.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program to get data fro customer table and insert in database.
'''

class CustomerDetails:
    @staticmethod
    def customer_data_insert():
        '''
        Title: method to get data and insert into customer table
        args: connection and last customer id
        return: None
        '''
        connection = db.get_connection()
        max_customer_id = util.select_data(cons.SELECT_MAX_CUSTOMER_QUERY, connection)
        if max_customer_id[0][0] == None:
            new_customer_id = 10000
        else:
            new_customer_id = max_customer_id[0][0] + 1
        #getting data to insert into table
        while True:
            row_data = ()
            row_data += (new_customer_id,)

            cus_first_name = input("Enter the first name of the customer: ")
            if util.check_null(cus_first_name) == False or len(cus_first_name) > 20:
                print("Customer first name cannot be empty and should be within 20 characters")
                continue
            row_data += (cus_first_name.strip(),)

            cus_last_name = input("Enter the last name of the customer: ")
            if len(cus_last_name) > 20:
                print("customer last name cannot be greater than 20 characters")
                continue
            row_data += (cus_last_name.strip(),)

            cust_address = input("Enter the address of the customer: ")
            if util.check_null(cust_address) == False or len(cust_address) > 200:
                print("Customer address cannot be empty and should be within 200 characters")
                continue
            row_data += (cust_address.strip(),)

            ph_number = input("Enter the phone number of the customer, if multiple separate by comma: ")
            if len(ph_number)  > 200 or  util.validate_phone_num(ph_number) == False:
                print("phone number is not valid")
                continue
            row_data += (ph_number.strip(),)

            email = input("Enter a valid email id: ")
            if len(email) > 30 or util.validate_email(email) == False:
                print("email id is not valid")
                continue
            row_data += (email.strip(),)

            dob = input("Enter a valid dob in the format dd/mm/yyyy: ")
            if util.get_date(dob) == False:
                print("dob is not valid")
                continue
            formatted_date = util.get_date(dob)
            row_data += (formatted_date,)

            
            print(cons.MARITAL_STATUS_LIST)
            marital_status = input("Enter your marital status, ONLY(Married, Single, Divorced)(case sensitive) : ")
            if marital_status.strip() not in cons.MARITAL_STATUS_LIST:
                print("Enter the marital status only as per the above list")
                continue
            row_data += (marital_status.strip(),)

            gender = input("enter your gender M/F: ")
            if util.check_gender(gender) == False:
                print("gender should be only as M or F and cannot be empty")
                continue
            row_data += (gender.strip(),)
            break
        print(row_data)
        #inserting the data into table
        try:
            cursor = connection.cursor()
            cursor.execute(cons.INSERT_CUSTOMER_QUERY, row_data)
            connection.commit()
            print("data inserted sucessfully")
        except Exception as ex:
            print(ex)
            connection.rollback()
        finally:
            db.close_cursor(cursor)
            db.close_connection(connection)
           


