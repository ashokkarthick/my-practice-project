import supplier as supp
import product as prod
import customer as cust
import purchase as pur
import sales as sal
import reports as rep
from common import constants as cons
from common import utilities as util
from common import database as db

'''
# @python program
# @Name: menu_drive.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program to create a menu driven operation.
'''

print("Available choices:")
print(" 1. Add Supplier data\n 2. Add Product data\n 3. Add Customer data\n 4. Add Purchase data\n 5. Add sales data\n 6. Calculate stock\n 7. Sales report\n 8. Exit the program")


#menu driven operation
while True:
    #getting choice from the user
    choice = input('Enter a choice as per the given options(1 to 8): ')
    if util.check_digit(choice) == False:
        print("Not a valid number")
        continue
    if int(choice) == 1:
        add_supplier = supp.SupplierDetails
        add_supplier.supplier_data_insert()
        continue
    elif int(choice) == 2:
        add_product = prod.ProductDetails
        add_product.product_data_insert()
        continue
    elif int(choice) == 3:
        add_customer = cust.CustomerDetails
        add_customer.customer_data_insert()
        continue 
    elif int(choice) == 4:
        add_purchase = pur.PurchaseDetails
        add_purchase.purchase_data_insert()
        continue
    elif int(choice) == 5:
        add_sales = sal.SalesDetails
        add_sales.insert_sales_data()
        continue
    if int(choice) == 6:
        get_stock = rep.GenerateReport
        get_stock.calculate_stock_details()
        continue
    elif int(choice) == 7:
        sales_date = input("Enter the date to generate sales report in the format dd/mm/yyyy: ")
        if util.get_date(sales_date) == False:
            print("date is not valid")
            continue
        formatted_date = util.get_date(sales_date)
        rep.GenerateReport.generate_sales_report(formatted_date)
        continue
    elif int(choice) == 8:
        break
    else:
        print("The choice {} is not available".format(choice))
        continue

    

