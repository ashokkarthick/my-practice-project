from common import constants as cons
from common import utilities as util
from common import database as db

'''
# @python program
# @Name: product.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program to get data and insert into product table.
'''
class ProductDetails:
    @staticmethod
    def product_data_insert():
        '''
        Title: method to get producct data and insert in table
        args: db connetcion and last product id
        return: None
        '''
        #getting data for all columns
        connection = db.get_connection()
        max_product_id = util.select_data(cons.SELECT_MAX_PRODUCT_QUERY, connection)
        if max_product_id[0][0] == None:
            new_product_id = 5000
        else:
            new_product_id = max_product_id[0][0] + 1
        available_supplier_ids = util.select_data(cons.SELECT_SUPPLIER_QUERY, connection)
        while True:
            row_data = ()
            row_data += (new_product_id,)
            product_name = input("Enter the product name: ")
            if util.check_null(product_name) == False or len(product_name) > 150:
                print("product name cannot be empty and length should be within 150")
                continue
            product_name_trim = product_name.strip()
            row_data += (product_name_trim.lower(),)

            product_description = input("Enter the product description: ")
            if util.check_null(product_description) == False or len(product_description) > 2000:
                print("product description cannot be empty and length should be within 2000")
                continue
            row_data += (product_description,)

            supplier_id = input("Enter a valid supplier id: ")
            if util.check_digit(supplier_id) == False or (int(supplier_id),) not in available_supplier_ids:
                print("supplier id cannot be empty and should be available in the suppliers list")
                continue
            row_data += (supplier_id,)
            catalog_url = input("Enter the catalog url: ")
            if util.check_null(catalog_url) == False or len(catalog_url) > 50:
                print("catalog url cannot be empty and should be within 50 characters")
                continue
            row_data += (catalog_url,)
            break
            #inserting a row of data into table
        try:
            cursor = connection.cursor()
            cursor.execute(cons.INSERT_PRODUCT_QUERY, row_data)
            connection.commit()
            print("data inserted sucessfully")
        except Exception as ex:
            print(ex)
            connection.rollback()
        finally:
            db.close_cursor(cursor)
            db.close_connection(connection)




