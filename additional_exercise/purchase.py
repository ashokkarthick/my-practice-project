from common import constants as cons
from common import utilities as util
from common import database as db

'''
# @python program
# @Name: purchase.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program to get data for purchase table and insert in database.
'''

class PurchaseDetails:
    @staticmethod
    def purchase_data_insert():
        '''
        Title: function to get data and insert into purchase table
        args: connection and last order id
        return: None
        '''
        connection = db.get_connection()
        max_order_id = util.select_data(cons.SELECT_MAX_ORDER_QUERY,connection)
        print(max_order_id)
        if max_order_id[0][0] == None:
            new_order_id = 20000
        else:
            new_order_id = max_order_id[0][0] + 1
        available_product_ids = util.select_data(cons.SELECT_PRODUCT_QUERY, connection)
        while True:
            row_data = ()
            row_data += (new_order_id,)
            if len(available_product_ids) == 0:
                print('No product ids available in product table to purchase')
                break
            product_id = input("Enter a valid product id: ")
            if util.check_digit(product_id) == False or (int(product_id),) not in available_product_ids:
                print("product id cannot be empty and should be available in the above product id list")
                continue
            row_data += (product_id,)

            time_id = input("Enter the date of purchase of the product: ")
            if util.get_date(time_id) == False:
                print("Time of purchase cannot be empty and should be in format dd/mm/yyyy")
                continue
            formatted_date = util.get_date(time_id)
            row_data += (formatted_date,)

            unit_price = input("Enter the unit price of the product: ")
            if util.check_decimal_value(unit_price,8) == False:
                print("Unit price should be valid number or decimal value")
                continue
            row_data += (round(float(unit_price), 2),)

            quantity = input("Enter the quantity of the product to be purchased: ")
            if util.check_digit(quantity) == False:
                print("Quantity should be numbers and cannot be empty")
                continue
            row_data += (int(quantity.strip()),)

            warranty_period = input("Enter the warranty period of the product: ")
            if util.check_digit(warranty_period) == False:
                print("warranty period cannot be empty and should be in numbers")
                continue
            row_data += (int(warranty_period.strip()),)
            break

        try:
            cursor = connection.cursor()
            cursor.execute(cons.INSERT_PURCHASE_QUERY, row_data)
            connection.commit()
            print("data have been successfully inserted")
        except Exception as ex:
            print(ex)
            connection.rollback()
        finally:
            db.close_cursor(cursor)
            db.close_connection(connection)








        

