from common import utilities as util
import common.constants as cons
import common.database as db

'''
# @python program
# @Name: reports.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program to generate stocks and sales reports.
'''

class GenerateReport:
    @staticmethod
    def calculate_stock_details():
        '''
        Title: method to calculate stock
        args: connection
        return: stock report
        '''
        connection = db.get_connection()
        #getting required data from purchase table
        purchase_data = util.select_data(cons.SELECT_PURCHASE_STOCK_QUERY, connection)
        #getting required data from sales table
        sales_data = util.select_data(cons.SELECT_SALES_STOCK_QUERY, connection)
        db.close_connection(connection)

        stock_report = []
        for lp in purchase_data:
            #getting the sum of sales quanity for the required product id
            sales_quantity = util.get_sales_quantity(lp[0], sales_data)
            stock_report.append({
                "productId" : lp[0],
                "productName" : lp[1],
                "purchased": lp[2],
                "sales" : float(sales_quantity),
                "inStock": float(lp[2]) - float(sales_quantity)
            })
    
        print('productId \t productName \t purchased \t sales \t instock \t')
        print("----------------------------------------------------------------------------------------")
        for lp in stock_report:
            print(lp.get("productId"), "\t\t", lp.get('productName'), "\t\t", lp.get("purchased"), "\t\t", lp.get("sales"), "\t\t", lp.get("inStock"))

        

    @staticmethod
    def generate_sales_report(sales_date):
        '''
        Title: method to get the sales report
        args: connection and date of sales
        return: None
        '''
        #fetching sales_dates and matching with input data
        connection = db.get_connection()
        sales_dates_list = util.select_data(cons.SELECT_SALES_DATES_QUERY,connection)
        if (sales_date,) not in sales_dates_list:
             print("no sales data available for given date")
        else: 
            sales_report_data = []
            try:
                cursor=connection.cursor()
                #fetching required sales report data from sales table
                cursor.execute(cons.SELECT_SALES_REPORT_QUERY,(sales_date,))
                print("data fetched")
                sales_report_data = cursor.fetchall()
            except Exception as ex:
                print(ex)
            finally:
                db.close_cursor(cursor)
                db.close_connection(connection)

            sales_report = []
            for lp in sales_report_data:
                sales_report.append({
                    "productId" : lp[0],
                    "productName" : lp[1],
                    "sales" : lp[2],
                    "numberOfCustomer" : lp[3]
                })
            print('productId \t productName \t sales \t numberOfCustomer')
            print("-------------------------------------------------------------------------")
            for lp in sales_report:
                print(lp.get("productId"), "\t\t", lp.get("productName"), "\t\t", lp.get("sales"), "\t\t", lp.get("numberOfCustomer"))

            
