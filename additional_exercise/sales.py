from common import constants as cons
from common import utilities as util
from common import database as db

'''
# @python program
# @Name: sales.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program to get data for sales table and insert in db.
'''

class SalesDetails:
    @staticmethod
    def insert_sales_data():
        '''
        Title: function to get data and insert into sales table
        args: None
        return: None
        '''
        #getting db connection
        connection = db.get_connection()
        #getting purchased product ids
        purchased_product_ids = util.select_data(cons.SELECT_PURCHASE_QUERY, connection)
        #getting available customer ids
        available_customer_ids = util.select_data(cons.SELECT_CUSTOMER_QUERY, connection)
        while True:
            row_data = ()
            #breaking the loop if no product ids  availble in purchase tables for sales    
            if len(purchased_product_ids) == 0:
                print("no products available in purchase table for sales")
                break
            product_id = input("Enter a valid product id from the above list: ")
            if util.check_digit(product_id) == False or (int(product_id),) not in purchased_product_ids:
                print("product id cannot be empty and should be available in the purchased product id list")
                continue
            product_id_trim =  product_id.strip()
            row_data += (product_id_trim,)
            #breaking the loop if no customer ids(mandatory) availble in customer table for sales
            if len(available_customer_ids) == 0:
                print("No customer ids available in customer table for sales")
                break
            customer_id = input("enter a valid customer id from the above list: ")
            if util.check_digit(customer_id) == False or (int(customer_id),) not in available_customer_ids:
                print("customer id cannot be empty and should be available in the customer id list")
                continue
            row_data += (customer_id,)
            time_id = input('Enter the time id of sales: ')
            if util.get_date(time_id) == False:
                print("Time is cannot be empty and should be in the format dd/mm/yyyy")
                continue
            formatted_date = util.get_date(time_id)
            row_data += (formatted_date,)
            channel_id = input("Enter the channel Id : ")
            if util.check_digit(channel_id) == False:
                print("channel id should be in numbers and cannot be empty")
                continue
            row_data += (channel_id.strip(),)
            promo_id = input("Enter the promo id: ")
            if util.check_digit(promo_id) == False:
                print("promo id cannot be empty and should be in numbers")
                continue
            row_data += (promo_id.strip(),)

            try:
                quantity_cursor = connection.cursor()
                #getting unit price and sum of purchased quantity for given product id from purchase table
                quantity_cursor.execute(cons.SELECT_UNIT_PRICE_QUANTITY_FROM_PURCHASE_QUERY,(product_id_trim,))
                unit_price_and_quantity = quantity_cursor.fetchall()
                quantity_sold_cursor = connection.cursor()
                #getting sum of quantity sold for given prod id from sales table
                quantity_sold_cursor.execute(cons.SELECT_QUANTITY_FROM_SALES_QUERY,(product_id_trim,))
                product_quantity_sold = quantity_sold_cursor.fetchone()
            except Exception as ex:
                print(ex)
            finally:
                db.close_cursor(quantity_cursor)
                db.close_cursor(quantity_sold_cursor)

            for loop in product_quantity_sold:
                if loop == None:
                    loop = 0
                quantity_already_sold  = loop
            for loop in unit_price_and_quantity:
                unit_price  = loop[0]
                quantity_purchased  = loop[1]
            print(unit_price_and_quantity)
            quantity_sold = input("Enter the quantity of items sold: ")
            if util.check_decimal_value(quantity_sold,10) == False or float(quantity_sold) > float(quantity_purchased - quantity_already_sold):
                print("available stock ", float(quantity_purchased - quantity_already_sold))
                continue
            quantity_sold = round(float(quantity_sold),2)
            row_data += (quantity_sold,)

            amount_sold = float(unit_price) *  float(quantity_sold)
            row_data += (amount_sold,)
            break
        #inserting the data into database
        try:
            cursor = connection.cursor()
            cursor.execute(cons.INSERT_SALES_QUERY, row_data)
            connection.commit()
        except Exception as ex:
            print(ex)
            connection.rollback()
        finally:
            db.close_cursor(cursor)
            db.close_connection(connection)






            




