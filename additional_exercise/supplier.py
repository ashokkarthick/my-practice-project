from common import constants as cons
from common import utilities as util
from common import database as db

'''
# @python program
# @Name: supplier.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program to get data and insert into supplier table.
'''
class SupplierDetails:
    @staticmethod
    def supplier_data_insert():
        '''
        Title: method to get data and insert into supplier table
        args: db connection and last supplier data
        return: None
        '''
        connection = db.get_connection()
        max_supplier_id = util.select_data(cons.SELECT_MAX_SUPPLIER_QUERY, connection)
        if max_supplier_id[0][0] == None:
            new_supplier_id = 1000
        else:
            new_supplier_id = max_supplier_id[0][0] + 1

        while True:
            row_data = ()
            row_data += (new_supplier_id,)

            supplier_name = input("Enter the supplier name(mandatory): ")
            return_value = util.check_null(supplier_name)
            if return_value == False or len(supplier_name) > 300:
                print("supplier name is not valid")
                continue
            row_data += (supplier_name,)
            
            supplier_address = input("Enter the supplier address(mandatory): ")
            return_value = util.check_null(supplier_address)
            if return_value == False or len(supplier_address) > 2000:
                print("supplier address is not valid")
                continue
            row_data += (supplier_address,)
            break

        try:
            cursor = connection.cursor()
            cursor.execute(cons.INSERT_SUPPLIER_QUERY, row_data)
            connection.commit()
        except Exception as ex:
            print(ex)
            connection.rollback()
        finally:
            db.close_cursor(cursor)
            db.close_connection(connection)    