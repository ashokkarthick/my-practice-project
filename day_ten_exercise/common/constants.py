'''
# @python program
# @Name: constants.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program that has constant values that run through the entire exercise like queries.
'''
DATABASE_ADDRESS = "D:\Ashok\sqlite-tools-win32-x86-3340100\IDP.db"

CREATE_IDPINSTITUTE_QUERY = "create table IDPInstitute(INSTITUTION_CODE int NOT NULL, INSTITUTION_NAME VARCHAR NOT NULL, ISACTIVE VARCHAR, COUNTRY VARCHAR, TYPE_OF_INSTITUTION VARCHAR NOT NULL, INSTITUTION_IMAGE_IRL VARCHAR NOT NULL, INSTITUTION_URL VARCHAR,"
CREATE_IDPINSTITUTE_QUERY += "PROFILE_EXISTS VARCHAR, RANKING int, ADDRESS VARCHAR, CONTACT_NUMBER VARCHAR, STATE VARCHAR, LATITUDE VARCHAR, LONGITUDE VARCHAR, TESTIMONIALS VARCHAR NOT NULL, EMAIL VARCHAR NOT NULL)"

INSERT_IDPINSTITUTE_QUERY = 'insert into IDPInstitute values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'

UPDATE_IDPINSTITUTE_QUERY = 'update IDPInstitute set INSTITUTION_CODE = ?, INSTITUTION_NAME = ?, ISACTIVE = ?, COUNTRY = ?,'
UPDATE_IDPINSTITUTE_QUERY += 'TYPE_OF_INSTITUTION = ?, INSTITUTION_IMAGE_IRL = ?, INSTITUTION_URL = ?, PROFILE_EXISTS = ?,'
UPDATE_IDPINSTITUTE_QUERY  += 'RANKING = ?, ADDRESS = ?, CONTACT_NUMBER = ?, STATE = ?, LATITUDE = ?, LONGITUDE = ?, TESTIMONIALS = ?, EMAIL = ? where INSTITUTION_CODE = ?'

SELECT_IDPINSTITUTE_QUERY = "select INSTITUTION_CODE, INSTITUTION_NAME, EMAIL, INSTITUTION_URL, COUNTRY from IDPInstitute"

SELECT_EMAIL_FROM_IDPINSTITUTE_QUERY = 'select EMAIL from IDPInstitute where ISACTIVE = ?'

CREATE_MOVIEGROUP_QUERY = 'create table MovieGroup(GROUP_ID NUMBER NOT NULL, GROUP_NAME VARCHAR NOT NULL)'

INSERT_MOVIEGROUP_QUERY = "insert into MovieGroup(GROUP_ID, GROUP_NAME) values(?, ?)"

SELECT_MOVIEGROUP_QUERY = 'select GROUP_ID, GROUP_NAME from MovieGroup'

CREATE_MOVIE_QUERY = 'create table Movie(MOVIE_ID NUMBER NOT NULL, MOVIE_NAME VARCHAR NOT NULL,'
CREATE_MOVIE_QUERY += ' ACTOR_NAME VARCHAR NOT NULL, GROUP_ID NUMBER NOT NULL)'

INSERT_MOVIE_QUERY = "insert into Movie values(?, ?, ?, ?)"

SELECT_MOVIE_QUERY = 'select MOVIE_ID, MOVIE_NAME, ACTOR_NAME, GROUP_ID from Movie'





