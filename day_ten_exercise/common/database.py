import sqlite3
from common import constants as cons

'''
# @python program
# @Name: database.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program that has functions for open, close database connections and close cursor
'''

def connect_database():
    '''
    Title: function to open the db connection
    args: None
    return: database connection
    '''
    database = sqlite3.connect(cons.DATABASE_ADDRESS)
    return database

def close_database(database):
    '''
    Title: function to close the connection
    args: db connection
    return: None
    '''
    if database is not None:
        database.close()

def close_cursor(cursor):
    '''
    Title: function to close the cursor
    args: cursor object
    return: None
    '''
    if cursor is not None:
        cursor.close()
    

