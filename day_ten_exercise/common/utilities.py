from common import database as db

'''
# @python program
# @Name: utilities.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program that has utility methods for the given exercises.
'''

def check_isdigit(str_data):
    '''
    Title:function to check the intitution code is integer or not and to do nullcheck
    args: str_data
    return: boolean status
    '''
    status = False
    if str_data is not None:
        if " " in str_data:
            str_data = str_data.replace(" ", "")
        if str_data.isdigit():
            status = True
    return status

def check_none_empty(check_data):
    """
    Checks if obtained data is non or null.
    :param check_data:
    :return: Boolean
    """
    check_status = False
    if len(check_data) != 0 and check_data is not None:
        check_status = True
    return check_status

def check_activity_status(str_data):
    '''
    Title: check whether the isactive data is yes or no
    args: str_data
    return: boolean status
    '''
    status = False
    if str_data is not None and str_data != "":
        if " " in str_data:
            str_data = str_data.replace(" ", "")
        if str_data.upper() == 'YES' or str_data.upper() == "NO":
            status = True
    return status

def print_emails(emails_list):
    '''
    Title: function to get the emails list and print it in tuple
    args: emails_list
    return: None
    '''
    emails = ()
    for lp in emails_list:
        emails += (lp[0],)
    print(emails)

def validate_email(email_data):
    '''
    Title: function to validate the email
    args: email string data
    return: boolean status
    '''
    status = False
    if email_data is not None and email_data != "":
        email_data = email_data.strip()
        if " " not in email_data and email_data.count("@") == 1 and  3 > email_data[email_data.index("@"):].count(".") > 0 and email_data[-1] != ".":
            status = True
    return status


def create_table(create_table_query, connection):
    '''
    Title: to create table
    args: create table query and db connection
    return: None
    '''
    try:
        #creating a cursor object
        cursor = connection.cursor()
        cursor.execute(create_table_query)
        print("table created successfully")

    except Exception as ex:
        print("The error is: ", ex)
        connection.rollback()
    
    finally:
        db.close_cursor(cursor)




