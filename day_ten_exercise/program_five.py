from common import database as db
from common import constants as cons
from common.utilities import create_table

'''
# @python program
# @Name: program_five.py
# @author: Ashok M
# @since: March 2021
# @ version: 1.0
# @see: program to create the given table and insert given data

'''
connection = db.connect_database()

movie_group_data= [
    (1000, "ACTION_MOVIE"),
    (1001, "COMEDY_MOVIE"), 
    (1002, 'FAMILY_MOVIE'), 
    (1003, 'ROMANCE'), 
    (1004, 'CHILDREN'),
    (1005, 'HORROR'),
    (1006, 'DOCUMENTARY'),
    (1007, 'SUSPENSE/THRILLER')
    ]

#calling the create_table function
create_table(cons.CREATE_MOVIEGROUP_QUERY, connection)

try:
    cursor = connection.cursor()
    #inserting the data into movie group table
    for lp in movie_group_data:
        cursor.execute(cons.INSERT_MOVIEGROUP_QUERY,lp)
        connection.commit()
    print("records have been successfully inserted")

except Exception as error:
    print("The error is: ", error)
    connection.rollback()

finally:
    db.close_cursor(cursor)
    print("cursor closed")
    db.close_database(connection)
    print("database closed")
