from common import database as db
from common.utilities import check_activity_status, print_emails
import common.constants as cons

'''
# @python program
# @Name: program_four.py
# @author: Ashok M
# @since: March 2021
# @ version: 1.0
# @see: program to read a flag and get the isactive column data and print the email address in tuple using function
'''

connection = db.connect_database()
#reading a flag
while True:
    flag = input("Enter the flag, either yes or no: ")
    return_value = check_activity_status(flag)
    if return_value == False:
        print("flag should be only yes or no")
        continue
    break

try:
    cursor = connection.cursor()
    #getting all emails from the database where isactive status matches the flag
    cursor.execute(cons.SELECT_EMAIL_FROM_IDPINSTITUTE_QUERY, (flag.upper(),))
    email_data = cursor.fetchall()
    #calling function that exists in another package to print the emails in tuple
    print_emails(email_data)
except Exception as error:
    print("The error is: ", error)
    connection.rollback()
finally:
    db.close_cursor(cursor)
    print("cursor closed")
    db.close_database(connection)
    print("connection closed")


