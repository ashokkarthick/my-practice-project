from common import database as db
from common import constants as cons

'''
# @python program
# @Name: program_one.py
# @author: Ashok M
# @since: March 2021
# @ version: 1.0
# @see: program to create a table with the given columns
'''

connection = db.connect_database()

try:
    #creating a cursor object
    cursor = connection.cursor()
    #creating idpinstitute table
    cursor.execute(cons.CREATE_IDPINSTITUTE_QUERY)
    print("table created successfully")

except Exception as ex:
    print("The error is: ", ex)
    connection.rollback()

finally:
    db.close_cursor(cursor)
    print("cursor closed")
    db.close_database(connection)
    print("connection closed")
    