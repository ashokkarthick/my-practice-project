from common import database as db
from common import constants as cons

'''
# @python program
# @Name: program_seven.py
# @author: Ashok M
# @since: March 2021
# @ version: 1.0
# @see: program to get the given columns in a dictionary
'''

connection = db.connect_database()

def get_movie_details(movie_data, movie_group_details):
    '''
    Title: to get movie details as a array of dictionaries
    args: movie_data list and movie_group_details
    return: movie_details list
    '''
    movie_details = []
    for lp in movie_data:
        movie_details.append(
            {
                'movieId':lp[0], 
                'movieName':lp[1], 
                'actorName':lp[2], 
                'groupName':movie_group_details.get(lp[3],"")
            }
        )
    return movie_details
 
try:
    # To fetch all records from movie table
    cursor = connection.cursor()
    cursor.execute(cons.SELECT_MOVIE_QUERY)
    movie_data = cursor.fetchall()
    # To fetch all records from moviegroup table
    cursor.execute(cons.SELECT_MOVIEGROUP_QUERY)
    movie_group_data = cursor.fetchall()
except Exception as e:
    print("Something went wrong ", e)
    connection.rollback()
finally:
    db.close_cursor(cursor)
    print("cursor closed")
    db.close_database(connection)
    print("connection closed")
 
# Convert all records which we taken from moviegroup table to dictionary values
movie_group_details = {}
for lp in movie_group_data:
    movie_group_details.update({lp[0]:lp[1]})
 
# Call a function
print(get_movie_details(movie_data, movie_group_details))