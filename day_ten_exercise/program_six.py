from common import database as db
import common.constants as cons
from common.utilities import check_isdigit, check_none_empty, create_table

'''
# @python program
# @Name: program_six.py
# @author: Ashok M
# @since: March 2021
# @version: 1.0
# @see: program to create a table named Movie and insert 10 data into it, get group_id that only exists in MovieGroup table
'''

connection = db.connect_database()

create_table(cons.CREATE_MOVIE_QUERY, connection)

try:
    cursor = connection.cursor()
    #select data from movie group
    cursor.execute(cons.SELECT_MOVIEGROUP_QUERY)
    movie_group_data = cursor.fetchall()
except Exception as e:
    print("the error is: ", e)
    connection.rollback()
finally:
    db.close_cursor(cursor)

#getting all group id from the movie_group_data in a list
group_ids = []
for lp in movie_group_data:
    group_ids.append(lp[0])
print("Available group ids: ", group_ids)

insertion_count = 0
while insertion_count < 10:
    #getting 10 values for all the 4 columns of the movie table
    row_data = ()
    movie_id = input("Enter the movie id in numbers: ")
    return_value = check_isdigit(movie_id)
    if return_value == False:
        print("Movie id should be only in numbers and cannot be empty")
        continue
    row_data += (movie_id,)

    movie_name = input("Enter a movie name(mandatory): ")
    return_value = check_none_empty(movie_name)
    if return_value == False:
        print("movie name cannot be empty")
        continue
    row_data += (movie_name,)

    actor_name = input("enter the actor name(mandatory): ")
    return_value = check_none_empty(actor_name)
    if return_value == False:
        print("Actor name cannot be empty")
        continue
    row_data += (actor_name,)

    group_id = input("Enter the group id: ")
    return_value = check_isdigit(group_id)
    if return_value == True and int(group_id) in group_ids:
        row_data += (group_id,)
    else:
        print("group id should be valid and should only be in numbers, breaking loop !!!")
        break

    try:
        cursor = connection.cursor()
        #inserting the data into movie table
        cursor.execute(cons.INSERT_MOVIE_QUERY, row_data)
        connection.commit()
        print("data have been inserted sucessfully")
        insertion_count += 1
    except Exception as ex:
        print("The error is: ", ex)
        connection.rollback()
    finally:
        db.close_cursor(cursor)

#closing the database
db.close_database(connection)
print("connection closed")