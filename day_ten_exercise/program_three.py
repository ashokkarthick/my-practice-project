from common import database as db
from common import constants as cons

'''
# @python program
# @Name: program_three.py
# @author: Ashok M
# @since: March 2021
# @ version: 1.0
# @see: program to get the given columns in a dictionary
'''

connection = db.connect_database()

try:
    cursor = connection.cursor()
    #selecting data from idpinstitute table
    cursor.execute(cons.SELECT_IDPINSTITUTE_QUERY)
    idp_institute_data = cursor.fetchall()
except Exception as e:
    print("The error is: ", e)
    connection.rollback()
finally:
    db.close_cursor(cursor)
    print("cursor closed")
    db.close_database(connection)
    print("database closed")


def institution_details(institution_data):
    '''
    Title: function to get the array of dictionaries
    args: instition data list
    return: institution_list
    '''
    try:
        institution_list = []
        for lp in institution_data:
            #appending the dictionary to institution list
            institution_list.append(
                {
                    'institutionCode':lp[0],
                    'institutionName':lp[1],
                    'email':lp[2],
                    'institutionUrl':lp[3], 
                    'country':lp[4]
                }
            )
        return institution_list
    except Exception as ex:
        return ex


print(institution_details(idp_institute_data))












            

