import sqlite3
import common.database as db
from common.utilities import check_isdigit, check_none_empty, check_activity_status, validate_email
import common.constants as cons

'''
# @python program
# @Name: program_two.py
# @author: Ashok M
# @since: March 2021
# @ version: 1.0
# @see: program to get 10 records for the 16 columns of IDPInstitute table and update the record if institution
        already exists based on the INSTITUTION_CODE
'''

connection = db.connect_database()

institution_codes = []
insertion_count = 0
while insertion_count < 10:
    row_data = ()
    #getting 10 inputs for all 16 columns
    ins_code = input("Enter the Institution Code(mandatory): ")
    return_value = check_isdigit(ins_code)
    if return_value == False:
        print("institution code cannot be empty and should only have numbers")
        continue
    else:
        row_data += (ins_code,)

    ins_name = input("Enter the institution name(mandatory): ")
    return_value = check_none_empty(ins_name)
    if return_value == False:
        print("Institution name cannot be empty")
        continue
    else:
        row_data += (ins_name,)

    is_act = input("is the institution active? Yes or No  ")
    return_value = check_activity_status(is_act)
    if return_value == False:
        print("activity status should only be yes or no")
        continue
    else:
        row_data += (is_act.upper(),)

    country = input("Enter the country: ")
    row_data += (country,)

    ins_type = input("Enter the type of the institution(mandatory): ")
    return_value = check_none_empty(ins_type)
    if return_value == False:
        print("Institution type cannot be empty")
        continue
    else:
        row_data += (ins_type,)

    ins_image = input("Enter the institution image irl(mandatory): ")
    return_value = check_none_empty(ins_image)
    if return_value == False:
        print("Institution image irl cannot be empty")
        continue
    else:
        row_data += (ins_image,)

    ins_url = input("Enter the institution url: ")
    row_data += (ins_url,)

    prof_exists = input("Is the profile exixts? ")
    row_data += (prof_exists,)

    ranking = input("Enter the rank of the institution: ")
    return_value = check_none_empty(ranking)
    if return_value == False:
        row_data = (ranking,)
    else:
        return_value = check_isdigit(ranking)
        if return_value == False:
            print("rank value should only be integer")
            continue
        else:
            row_data += (ranking,)

    address = input("Enter the address of the institution: ")
    row_data += (address,)

    con_num = input("Enter the contact number: ")
    row_data += (con_num,)

    state = input("Enter the state: ")
    row_data += (state,)

    latitude = input("Enter the latitude: ")
    row_data += (latitude,)

    longitude = input("Enter the longitude: ")
    row_data += (longitude,)

    testimonials = input('Enter the testimonial(mandatory): ')
    return_value = check_none_empty(testimonials)
    if return_value == False:
        continue
    else:
        row_data += (testimonials,) 

    email = input("Enter the email id(mandatory): ")
    return_value = validate_email(email)
    if return_value == False:
        print("email Id is not valid")
        continue
    else:
        row_data += (email,)

    #check whether the institution_code exists in institution_codes list, if not exists, inserting data
    if row_data[0] not in institution_codes:
        try:
            cursor = connection.cursor()
            cursor.execute(cons.INSERT_IDPINSTITUTE_QUERY,row_data)
            connection.commit()
            print("data inserted")
            institution_codes.append(row_data[0])
            insertion_count += 1
        except Exception as ex:
            print("The error during insertion is: ", ex)
            connection.rollback()
        finally:
            db.close_cursor(cursor)

    #if institution code exists, updating data
    else:
        try:
            cursor= connection.cursor()
            row_data += (row_data[0],)
            cursor.execute(cons.UPDATE_IDPINSTITUTE_QUERY,row_data)
            connection.commit()
            print("institution already exists, so data updated")
        except Exception as error:
            print("The error during updation is: ", error)
            connection.rollback()
        finally:
            db.close_cursor(cursor)

db.close_database(connection)
print("database connection closed")